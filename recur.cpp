//Returns the product of its inputs

#include <iostream>

using namespace std;

int recur(int a, int b) 
{ 
    if (a == 0 || b == 0)
	{
		return 0;
	}
	
	if (b==1)
    {
        return a;
    }
	else
	{
		return a + recur(a, b - 1);
	}
}




int main()
{
	int a;
	int b;
	int result;
	 cout << "Enter two integers to multiply" << endl; //prompt user to input 2 integers
     cin >> a >> b; //store them in variable a and b
     
     result = recur(a,b);
     cout << result<<endl;
 }
